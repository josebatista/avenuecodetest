package br.com.avenuecode.steps;

import java.util.ResourceBundle;

import org.openqa.selenium.WebDriver;

import br.com.avenuecode.config.TestConfiguration;
import br.com.avenuecode.page.NavBar;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	private ResourceBundle bundle = ResourceBundle.getBundle("application");
	private TestConfiguration testConfig;
	
	public Hooks(TestConfiguration testConfig) {
		this.testConfig = testConfig;
	}
	
	@Before
	public void initialize (){
		WebDriver driver = this.testConfig.getDriver();
		driver.navigate().to(this.bundle.getString("url"));
		new NavBar(driver).clickSigin();
	}

	
	@After
	public void tearDown () {
		this.testConfig.close ();
	}
	
	
}
