package br.com.avenuecode.steps;

import org.junit.Assert;

import br.com.avenuecode.config.TestConfiguration;
import br.com.avenuecode.page.NavBar;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class NavBarSteps {

	private NavBar navBar;
	
	public NavBarSteps (TestConfiguration testConfig) {
		this.navBar = new NavBar(testConfig.getDriver());
	}
		
	@Given("^I can see 'My Tasks' link on the NavBar$")
	public void iCanSeeMyTasksLinkOnTheNavbar () {
		Assert.assertTrue("The link 'My Tasks is not visible on NavBar'", navBar.hasOption("My Tasks"));
	}
	
	@When("^I click in 'My Tasks' link on the NavBar$")
	public void iClickInMyTasksLinkOnTheNavBar() throws Throwable {
	    this.navBar.clickMyTasks();
	}
	
}
