package br.com.avenuecode.steps;


import java.util.ResourceBundle;

import org.junit.Assert;

import br.com.avenuecode.config.TestConfiguration;
import br.com.avenuecode.page.HomePage;
import br.com.avenuecode.page.LoginPage;
import cucumber.api.java.en.Given;

public class LoginSteps {

	private LoginPage loginPage; 
	private HomePage homePage;
	private ResourceBundle bundle;
	
	public LoginSteps(TestConfiguration testConfiguration) {
		this.loginPage = new LoginPage(testConfiguration.getDriver());
		this.homePage = new HomePage(testConfiguration.getDriver()); 
		this.bundle = ResourceBundle.getBundle("application");
	}
	
	@Given ("^I am logged as ToDo App user$")
	public void iAmLoggedAsTodoAppUser () {
		this.loginPage.setEmail(bundle.getString("username"));
		this.loginPage.setPassword(bundle.getString("password"));
		this.loginPage.sigIn();
	}
	
	@Given ("^I am logged as \"([^\"]*)\" on ToDO App$")
	public void iAmLoggedAsOnToDoApp (String username) {
		if (!this.homePage.onPage()) {
			this.loginPage.setEmail(bundle.getString("username"));
			this.loginPage.setPassword(bundle.getString("password"));
			this.homePage = this.loginPage.sigIn();
		}
		String welcomeMessage = homePage.navBar().getWelcomeMessage().getText();
		Assert.assertTrue(String.format("The user logged is not the \"%s\"", username),welcomeMessage.contains(username));
	}
}
