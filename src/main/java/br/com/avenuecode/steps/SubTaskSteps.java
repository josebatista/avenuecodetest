package br.com.avenuecode.steps;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import br.com.avenuecode.config.TestConfiguration;
import br.com.avenuecode.page.MyTasksPage;
import br.com.avenuecode.page.SubTasksPage;
import br.com.avenuecode.utils.TextUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SubTaskSteps {

	private MyTasksPage myTaskPage;
	private SubTasksPage subTaskPage;
	
	public SubTaskSteps (TestConfiguration testConfig) {
		this.subTaskPage = new SubTasksPage (testConfig.getDriver());
		this.myTaskPage = new MyTasksPage(testConfig.getDriver());
	}
	
	
	@Given("^I have a task listed$")
	public void iHaveATaskListed() {
	    if (this.myTaskPage.getTasksDescription().isEmpty()){
	    	this.myTaskPage.setTaskDescription("New Task");
	    	this.myTaskPage.clickAddButton();
	    }
	    List <WebElement> tasks = this.myTaskPage.getTasks();
	    Assert.assertTrue ("The logged user has no task listed.",!tasks.isEmpty());
 	}
	
	
	@When("^I click at 'Manage Subtasks' button$")
	public void iClickAtManageSubtasksButton()  {
	    List <WebElement> tasks = this.myTaskPage.getTasks();
	    Assert.assertTrue ("The logged user has no task listed.",!tasks.isEmpty());
	    
	    WebElement task = tasks.get(0);
	    this.subTaskPage = this.myTaskPage.clickManageSubTaskButton(task);
	}
	
	
	@When("^I add a new subtask with the characteristics$")
	public void iAddANewSubtaskWithTheCharacteristics(DataTable dataTable) {
	    Objects.requireNonNull(dataTable,"A datable with 'description' and 'date' is required.");
	    Map <String, String> param = dataTable.asMaps(String.class, String.class).get(0);
	    this.iClickAtManageSubtasksButton();
	    this.subTaskPage.setSubTaskDescription(param.get("description"));
	    this.subTaskPage.setDueDate(param.get("date"));
	    this.subTaskPage.clickAddButton();
	}
	
	@Then("^I should see a read only field with Task description$")
	public void iShouldSeeAReadOnlyFieldWithTaskDescription()  {
	    WebElement taskDescriptionField = this.subTaskPage.getTaskDescription();
	    Assert.assertTrue("The 'ToDo' field at modal dialog containing the Task description should be read only.",!taskDescriptionField.isEnabled());
	}
	
	
	@Then("^I should not be able to input (\\d+) characters at 'SubTask Description' field$")
	public void iShouldNotBeAbleToInputCharactersAtSubTaskDescriptionField(int numberOfCharacters) {
	   String largeText = TextUtils.generateText(numberOfCharacters);
	   this.subTaskPage.setSubTaskDescription(largeText);
	   WebElement subTaskDescriptionField = this.subTaskPage.getSubTaskDescription();
	   Assert.assertTrue(String.format("The subtask with description '%s' should not be created. The system should only allow to insert 250 characters.", largeText),subTaskDescriptionField.getAttribute("value").length()<=250);
	}
	
	@Then("^I should not see a new subtask with description \"([^\"]*)\" appended on the botton part of modal$")
	public void iShouldNotSeeANewSubtaskWithDescriptionAppendedOnTheBottonPartOfModal(String subTaskDescription) {
	    List <WebElement> subTasksDescriptions = this.subTaskPage.getSubTasksDescription();
	    boolean hasTheSubtask = false;
	    for (WebElement subTaskDesc: subTasksDescriptions ){
	    	if (subTaskDesc.getText().trim().equalsIgnoreCase(subTaskDescription)) hasTheSubtask = true;
	    }
	    Assert.assertTrue(String.format("The SubTask with description '%s' should not be created.", subTaskDescription), !hasTheSubtask);
	}
	
	@Then("^I should see a new subtask with description \"([^\"]*)\" appended on the botton part of modal$")
	public void iShouldSeeANewSubtaskWithDescriptionAppendedOnTheBottonPartOfModal(String subTaskDescription) {
		List <WebElement> subTasksDescriptions = this.subTaskPage.getSubTasksDescription();
	    boolean hasTheSubtask = false;
	    for (WebElement subTaskDesc: subTasksDescriptions ){
	    	if (subTaskDesc.getText().trim().equalsIgnoreCase(subTaskDescription)) hasTheSubtask = true;
	    }
	    Assert.assertTrue(String.format("The SubTask with description '%s' was not created, but was expetecd to be.", subTaskDescription), hasTheSubtask);
	}

}
