package br.com.avenuecode.steps;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import br.com.avenuecode.config.TestConfiguration;
import br.com.avenuecode.page.MyTasksPage;
import br.com.avenuecode.utils.TextUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TaskSteps {

	private MyTasksPage myTasksPage;

	public TaskSteps(TestConfiguration testConfig) {
		this.myTasksPage = new MyTasksPage(testConfig.getDriver());
	}

	@Then("^I should be redirected to the page with all tasks created$")
	public void iShouldBeRedirectedToThePageWithAllTasksCreated() {
		Assert.assertTrue("The user was not redirected to the page with its tasks.", this.myTasksPage.onPage());
	}

	@Given("^I am on the My Tasks page$")
	public void iAmOnTheMyTasksPage() {
		this.myTasksPage.getNavBar().clickMyTasks();
		Assert.assertTrue("The user is not on the page with its tasks.", this.myTasksPage.onPage());
	}

	@When("^I insert a new task with name \"([^\"]*)\"$")
	public void iInsertANewTaskWithName(String description) {
	    this.myTasksPage.setTaskDescription(description);
	}
	
	@When("^I hit the Enter button$")
	public void iHitTheEnterButton() {
	    this.myTasksPage.pressEnter();
	}
	
	@Then("^I should see the message \"([^\"]*)\" on the top part$")
	public void iShouldSeeTheMessageOnTheTopPart(String message) {
		String toDoMessage = this.myTasksPage.getToDoMessage().getText();
		Assert.assertTrue(String.format("Expecting the message \"%s\" but got \"%s\"", message, toDoMessage),
				toDoMessage.equalsIgnoreCase(message));
	}
	
	
	
	@When("^I click on the add task button$")
	public void iClickOnTheAddTaskButton() {
		this.myTasksPage.clickAddButton();
	}
	
	@When("^I insert a new task with empty name$")
	public void iInsertANewTaskWithEmptyName()  {
	    this.myTasksPage.setTaskDescription("");
	}
	
	@When("^I insert a new task with (\\d+) characters$")
	public void iInsertANewTaskWithCharacters(int numberOfCharacters)  {
	    String largeDescription = TextUtils.generateText(numberOfCharacters);
	    this.myTasksPage.setTaskDescription(largeDescription);
	}
	
	
	@Then("^No new task with (\\d+) characters should be appended on the list of created tasks$")
	public void noNewTaskWithCharactersShouldBeAppendedOnTheListOfCreatedTasks(int numberOfCharacters)  {
		String taskDescription =TextUtils.generateText(numberOfCharacters);
		List <WebElement> taskDescriptions = this.myTasksPage.getTasksDescription();
	    boolean isTaskPresent = false;
	    for (WebElement taskDesc : taskDescriptions) {
	    	if (taskDesc.getText().trim().equalsIgnoreCase(taskDescription)) isTaskPresent = true;
	    }
	    Assert.assertTrue(String.format("The task with name '%s' should not be created.", taskDescription),!isTaskPresent);
	}


	
	@Then("^No new task with name \"([^\"]*)\" should be appended on the list of created tasks$")
	public void noNewTaskWithNameShouldBeAppendedOnTheListOfCreatedTasks(String taskDescription) {
	    List <WebElement> taskDescriptions = this.myTasksPage.getTasksDescription();
	    boolean isTaskPresent = false;
	    for (WebElement taskDesc : taskDescriptions) {
	    	if (taskDesc.getText().trim().equalsIgnoreCase(taskDescription)) isTaskPresent = true;
	    }
	    Assert.assertTrue(String.format("The task with name '%s' should not be created.", taskDescription),!isTaskPresent);
	}
	
	@Then("^No new task with empty name should be appended on the list of created tasks$")
	public void noNewTaskWithEmptyNameShouldBeAppendedOnTheListOfCreatedTasks()  {
		List <WebElement> taskDescriptions = this.myTasksPage.getTasksDescription();
	    boolean isTaskPresent = false;
	    for (WebElement taskDesc : taskDescriptions) {
	    	if (taskDesc.getText().trim().equalsIgnoreCase("Empty")) isTaskPresent = true;
	    }
	    Assert.assertTrue(String.format("No task should be add without typing a name."),!isTaskPresent);
	}

}
