package br.com.avenuecode.config;

import cucumber.runtime.java.picocontainer.PicoFactory;

/**
 * Class used to define which class will be inject at every stepDefinition
 *
 */
public class CustomPicoFactory extends PicoFactory{

	public CustomPicoFactory () {
		this.addClass(TestConfiguration.class);
	}
	
}
