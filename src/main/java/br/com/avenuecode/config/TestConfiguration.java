package br.com.avenuecode.config;

import java.util.ResourceBundle;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Class containing the test configuration like WebDriver.
 * 
 * This class is inject in every StepDefinition and Hooks classes
 *
 */
public class TestConfiguration {
	
	private ResourceBundle bundle;
	private WebDriver driver;
	
	
	public TestConfiguration () {
		this.bundle = ResourceBundle.getBundle("application");
		System.setProperty("webdriver.chrome.driver",this.bundle.getString("chromedriver.path"));
		this.driver = new ChromeDriver ();
	}
	
	/**
	 * Return the WebDriver used by the test
	 * @return Return the WebDriver used by the test
	 */
	public WebDriver getDriver () {
		return this.driver;
	}
	
	/**
	 * Quit the WebDriver after the test finish
	 */
	public void close () {
		this.driver.close();
	}
	
}
