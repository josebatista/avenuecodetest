package br.com.avenuecode.utils;

public class TextUtils {
	private TextUtils () {}
	
	/**
	 * Function to generate a text with the number of characters expected
	 * 
	 * @param numberOfCharacters number of characters expected of text generated
	 * @return a simple text with the number of characters specified
	 */
	public static String generateText (int numberOfCharacters) {
		String baseText = String.format("%dcharacters", numberOfCharacters);
		int times = numberOfCharacters / baseText.length();
		int plus = numberOfCharacters%baseText.length();
		StringBuilder sb = new StringBuilder("");
		for (int i =0; i<times; i++ ) {
			sb.append(baseText);
		}
		sb.append(baseText.substring(0, plus));
		return sb.toString();
	}
}
