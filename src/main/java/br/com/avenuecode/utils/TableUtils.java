package br.com.avenuecode.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TableUtils {
	
	private TableUtils (){}
	
	/**
	 * By the title associated to a table the method tries to find all rows on the table.
	 * 
	 * @param driver WebDriver been used in the test
	 * @param tableTitle the visible title associated to the table on screen
	 * @return A list of element corresponding to all tr elements found on the table.
	 */
	public static List <WebElement> getRows (WebDriver driver, String tableTitle) {
		String xpath = String.format("//*[contains(text(),'%s')]//following-sibling::table//descendant::tbody//descendant::tr", tableTitle);
		try {
			return driver.findElements(By.xpath(xpath));
		}
		catch (NoSuchElementException e) {
			throw new NoSuchElementException(String.format("No table could be found by the title '%s'. Xpath used: %s", tableTitle, xpath));
		}
	}
	
	/**
	 * Gets a list of element at a specific column of table.
	 * 
	 * @param driver WebDriver been used in the test
	 * @param tableTitle the visible title associated to the table on screen
	 * @param columName the name of column which the elements will be extracted
	 * @return A list of element corresponding to all td elements found on a specific column of the table.
	 */
	public static List <WebElement> getColumn (WebDriver driver, String tableTitle, String columnName) {
		String xpathToGetColumnIndex = String.format("count (//*[contains(text(),'%s')]//following-sibling::table//descendant::th)-count(//*[contains(text(),'%s')]//following-sibling::table//descendant::th[contains(text(),'%s')]//following-sibling::th)",tableTitle, tableTitle,columnName );
		String xpathToGetColumnElement = String.format("//*[contains(text(),'%s')]//following-sibling::table//descendant::tbody//descendant::td[position()=(%s)]", tableTitle, xpathToGetColumnIndex );
		try {
			return driver.findElements(By.xpath(xpathToGetColumnElement));
		}
		catch (NoSuchElementException e) {
			throw new NoSuchElementException (String.format("No column '%s' found at table '%s'. Xpath used %s", columnName, tableTitle, xpathToGetColumnElement));
		}
	}
	
	/**
	 * Returns a specific column of a specific row of a given table.
	 * 
	 * @param row WebElement representing a tr element of the given table.
	 * @param tableTitle The title associated to the table
	 * @param columnName The name of the desired column
	 * @return Return a webElement containing a cell of the table
	 */
	public static WebElement getColumnOfRow (WebElement row, String tableTitle, String columnName) {
		String xpathToGetColumnIndex = String.format("count (//*[contains(text(),'%s')]//following-sibling::table//descendant::th)-count(//*[contains(text(),'%s')]//following-sibling::table//descendant::th[contains(text(),'%s')]//following-sibling::th)",tableTitle, tableTitle,columnName );
		return row.findElement(By.xpath(String.format(".//td[position() = (%s) ]", xpathToGetColumnIndex)));
	}
}
