package br.com.avenuecode.page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavBar{

	private final String NAVBAR_OPTION_XPATH = "//div[contains(@class, 'navbar-default')]//descendant::a[contains(text(),'%s')]"; 
	private WebDriver driver;
	
	public NavBar(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Click at 'ToDo App' link on the NavBar
	 * @return return a HomePage object, which is the next one
	 */
	public HomePage clickToDoApp (){
		this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, "ToDo App"))).click();
		return new HomePage(driver);
	}
	
	/**
	 * Click at 'Home' link on the NavBar
	 * @return return a HomePage object, which is the next one
	 */
	public HomePage clickHome () {
		this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, "Home"))).click();
		return new HomePage(driver);
	}
	
	/**
	 * Click at 'My Tasks' link on the NavBar
	 * @return return a MyTasksPage object, which is the next one
	 */
	public MyTasksPage clickMyTasks () {
		this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, "My Tasks"))).click();
		return new MyTasksPage(driver);
	}
	
	/**
	 * Click at 'Sign In' link on the NavBar
	 * @return return a LoginPage object, which is the next one
	 */
	public LoginPage clickSigin () {
		this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, "Sign In"))).click();
		return new LoginPage(driver);
	}
	
	/**
	 * Gets the welcome message on the NavBar
	 * @return return a WebElement containing the message.
	 */
	public WebElement getWelcomeMessage () {
		return this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, "Welcome")));
	}
	
	/**
	 * Verifies if some link is visible on the NavBar
	 * @param navBarOption the NavBar option to be verified
	 * @return return true if the link is avaiable on NavBar, otherwise return false
	 */
	public boolean hasOption (String navBarOption) {
		try {
			this.driver.findElement(By.xpath(String.format(NAVBAR_OPTION_XPATH, navBarOption)));
			return true;
		}
		catch (NoSuchElementException e) {
			return false;
		}
	}
}
