package br.com.avenuecode.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import br.com.avenuecode.utils.TableUtils;

public class SubTasksPage extends Page{

	public SubTasksPage(WebDriver driver) {
		super(driver);
	}

	@Override
	public void waitToLoad() {
		String xpath = "//div[contains(@class,'modal-dialog')]";
		this.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}
	
	
	/**
	 * Return the field containg the Task description on modal dialog
	 * @return the field containg the Task description on modal dialog
	 */
	public WebElement getTaskDescription () {
		return this.driver.findElement(By.id("edit_task"));
	}
	
	/**
	 * Insert a text into 'SubTask Description' field
	 * @param subTaskDescription Subtask description to be inserted
	 */
	public void setSubTaskDescription (String subTaskDescription) {
		this.driver.findElement(By.id("new_sub_task")).sendKeys(subTaskDescription);;
	}
	
	/**
	 * Gets the 'SubTask Description' field
	 * 
	 * @return the 'SubTask Description' field
	 */
	public WebElement getSubTaskDescription () {
		return this.driver.findElement(By.id("new_sub_task"));
	}
	
	/**
	 * Insert a date already formatted into the 'Due date' field
	 * @param dueDate a date already formatted
	 */
	public void setDueDate (String dueDate) {
		WebElement dueDateField = this.driver.findElement(By.id("dueDate"));
		dueDateField.clear ();
		dueDateField = this.driver.findElement(By.id("dueDate"));
		dueDateField.sendKeys(dueDate);
	}
	
	/**
	 * Click at 'Add' button on dialog modal
	 */
	public void clickAddButton () {
		this.driver.findElement(By.id("add-subtask")).click();
	}

	/**
	 * Click at 'Close' button on dialog modal
	 */
	public void clickCloseButton () {
		this.driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();
	}
	
	/**
	 * Return all subtasks listed at 'SubTasks of this ToDo' table
	 * @return List of WebElements, which every element is a line (TR) of the 'SubTasks of this ToDo' table
	 */
	public List <WebElement> getSubTasks () {
		return TableUtils.getRows(driver, "SubTasks of this ToDo");
	}
	
	/**
	 * Return the description of all substasks listes at 'SubTasks of this ToDo' table
	 * 
	 * @return List of WebElements which every element is a line of the 'SubTask' column 
	 * of the 'SubTasks of this ToDo' table
	 */
	public List <WebElement> getSubTasksDescription () {
		return TableUtils.getColumn(driver, "SubTasks of this ToDo", "SubTask");
	}
}
