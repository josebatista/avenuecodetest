package br.com.avenuecode.page;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page{

	protected WebDriver driver;
	protected WebDriverWait driverWait;
	private NavBar navBar;
	
	public Page (WebDriver driver) {
		this.driver = driver;
		this.driverWait = new WebDriverWait(driver, 30);
		this.navBar = new NavBar(driver);
	}
	
	/**
	 * Wait until the page is loaded
	 */
	public abstract void waitToLoad ();
	
	/**
	 * Verify if is on desired page
	 * 
	 * @return return true if already on the page, otherwise return false
	 */
	public boolean onPage (){
		try {
			waitToLoad();
			return true;
		}
		catch (TimeoutException e) {
			return false;
		}
	};
	
	/**
	 * Return the NavBar
	 * @return Return the NavBar
	 */
	public NavBar getNavBar () {
		return this.navBar;
	}
	
	
	
}
