package br.com.avenuecode.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends Page{
	
	public LoginPage (WebDriver driver) {
		super (driver);
	}
	

	@Override
	public void waitToLoad() {
		this.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'panel-heading')]//descendant-or-self::*[contains (text(),'Sign in')]")));
	}
	
	/**
	 * Set the email into email field
	 * @param email email to be set
	 */
	public void setEmail (String email) {
		WebElement emailField = driver.findElement(By.id("user_email"));
		emailField.sendKeys(email);
	}
	
	/**
	 * Set the password into password field
	 * @param password password to be set
	 */
	public void setPassword (String password) {
		WebElement passField = driver.findElement(By.id("user_password"));
		passField.sendKeys(password);
	}
	
	/**
	 * Submit the form and verifies if the credentials are correct. 
	 * Returns the next page if the credentials are correct.
	 * @return {@link HomePage}
	 */
	public HomePage sigIn () {
		this.driver.findElement(By.xpath("//input[contains(@value, 'Sign in')]")).click();
		return new HomePage(driver);
	}

}
