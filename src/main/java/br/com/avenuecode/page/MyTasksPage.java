package br.com.avenuecode.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import br.com.avenuecode.utils.TableUtils;

public class MyTasksPage extends Page{

	public MyTasksPage(WebDriver driver) {
		super(driver);
	}

	@Override
	public void waitToLoad() {
		this.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains (@class, 'panel-default')]//descendant::*[contains(text(),'To be Done')]")));	
	}

	/**
	 * Return the WebElement with the message saying the Tasks belonging to the logged user. 
	 * @return WebElement with the message saying the Tasks belonging to the logged user.
	 */
	public WebElement getToDoMessage () {
		return this.driver.findElement(By.xpath("//div[contains (@class,'navbar-default')]//following-sibling::h1"));
	}

	/**
	 * Set the new task description
	 * @param description Description to be set
	 */
	public void setTaskDescription (String description) {
		this.driver.findElement(By.id("new_task")).sendKeys(description);
	}

	/**
	 * Simulate the user hitting enter
	 */
	public void pressEnter () {
		new Actions(driver).sendKeys(Keys.ENTER).build().perform();
	}
	
	/**
	 * Click at the Add button on My Tasks page
	 */
	public void clickAddButton () {
		this.driver.findElement(By.xpath("//input [@id='new_task']//following-sibling::span[contains (@class,'glyphicon-plus')]")).click();
	}
	
	/**
	 * Return the list of all WebElements containing their respective Task description 
	 * 
	 * @return list of all WebElements containing their respective Task description 
	 */
	public List <WebElement> getTasksDescription () {
		return TableUtils.getColumn(driver, "To be Done", "Todo");
	}
	
	/**
	 * Return the list of lines at 'To be Done' table.
	 * @return all lines at 'To be Done' tables
	 */
	public List <WebElement> getTasks () {
		return TableUtils.getRows(driver, "To be Done");
	}
	
	/**
	 * Click at the 'Manage Substasks' button of a specific task obtained by getTasks () function
	 * @param task A Task (tr element) of table the 'To be Done' obtained by getTasks () function
	 * @return a new SubTasksPage, which is the following page 
	 */
	public SubTasksPage clickManageSubTaskButton (WebElement task) {
		WebElement manageSubtasksButton = TableUtils.getColumnOfRow(task, "To be Done", "SubTasks");
		manageSubtasksButton.click();
		return new SubTasksPage(driver);
	}
	
}
