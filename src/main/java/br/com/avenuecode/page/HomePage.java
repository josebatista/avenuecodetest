package br.com.avenuecode.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePage extends Page{

	public HomePage (WebDriver driver) {
		super (driver);
	}
	
	public NavBar navBar () {
		return super.getNavBar();
	}

	@Override
	public void waitToLoad() {
		this.driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'jumbotron grey-jumbotron')]")));	
	}
	
}
