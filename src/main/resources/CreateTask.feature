Feature: Create Task
  
  As a ToDo App user
  I should be able to create a task
  So I can manage my tasks


	Background:
		Given I am logged as ToDo App user

	
  Scenario: Click in 'My Tasks' on NavBar
    Given I can see 'My Tasks' link on the NavBar
    When I click in 'My Tasks' link on the NavBar
    Then I should be redirected to the page with all tasks created

  @Bug 
  Scenario: Displayed message on the top part saying that list belongs to the logged user
    Given I am logged as "Jose Renato Batista Junior" on ToDO App
    And I am on the My Tasks page
    Then I should see the message "Hey Jose Renato Batista Junior, this is your todo list for today:" on the top part

	#-------------------------------------
	#Testing simple case for both kind of entries
	#-------------------------------------
	
  Scenario: Enter a new task by hitting enter
  	Given I am on the My Tasks page
  	When I insert a new task with name "New Task"
  	And I hit the Enter button
  	Then A new task with name "New Task" should be appended on the list of created tasks
  	
  Scenario: Enter a new task by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with name "New Task"
  	And I click on the add task button
  	Then A new task with name "New Task" should be appended on the list of created tasks
  	
  #-------------------------------------	
  #Testing minimun characters limits	
  #-------------------------------------
  
  Scenario: Enter a new task with three charecters by hitting Enter
  	Given I am on the My Tasks page
  	When I insert a new task with name "tsk"
  	And I hit the Enter button
  	Then A new task with name "tsk" should be appended on the list of created tasks
  	
  Scenario: Enter a new task with three charecters by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with name "tsk"
  	And I click on the add task button
  	Then A new task with name "tsk" should be appended on the list of created tasks
  	  	
  @Bug 
  Scenario: Enter a new task with less than three charecters by hitting Enter
  	Given I am on the My Tasks page
  	When I insert a new task with name "tk"
  	And I hit the Enter button
  	Then No new task with name "tk" should be appended on the list of created tasks
  	
  @Bug
  Scenario: Enter a new task with less than three charecters by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with name "tk"
  	And I click on the add task button
  	Then No new task with name "tk" should be appended on the list of created tasks
  
  Scenario: Enter a new task with empty name by hitting Enter
  	Given I am on the My Tasks page
  	When I insert a new task with empty name
  	And I hit the Enter button
  	Then No new task with empty name should be appended on the list of created tasks
  	  	
  @Bug 
  Scenario: Enter a new task with empty name by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with empty name
  	And I click on the add task button
  	Then No new task with empty name should be appended on the list of created tasks
  	
  #-------------------------------------	
  #Testing maximun characters limits	
  #-------------------------------------
  
  Scenario: Enter a new task with 250 characters by hitting Enter
  	Given I am on the My Tasks page
  	When I insert a new task with 250 characters
  	And I hit the Enter button
  	Then A new task with 250 characters should be appended on the list of created tasks
  	
  Scenario: Enter a new task with 250 characters by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with 250 characters
  	And I click on the add task button
  	Then A new task with 250 characters should be appended on the list of created tasks
  	
  @Bug 
  Scenario: Enter a new task with 251 characters by hitting Enter
  	Given I am on the My Tasks page
  	When I insert a new task with 251 characters
  	And I hit the Enter button
  	Then No new task with 251 characters should be appended on the list of created tasks
  	
  @Bug
  Scenario: Enter a new task with 251 characters by clicking on the add task button
  	Given I am on the My Tasks page
  	When I insert a new task with 251 characters
  	And I click on the add task button
  	Then No new task with 251 characters should be appended on the list of created tasks
	
	