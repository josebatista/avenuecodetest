@SubTask
Feature: Create subtask
  
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks in smaller pieces

  Background: 
    Given I am logged as ToDo App user

  Scenario: The Tasks should have a button labeled as 'Manage Subtasks'
    Given I am on the My Tasks page
    And I have a task listed
    Then I should see a button labeled as 'Manage Subtasks'

  Scenario: The button labeled as 'Manage Subtasks' should have the number of Subtasks
    Given I am on the My Tasks page
    And I have a task listed
    Then I should see a the number of Subtasks at the button labeled as 'Manage Subtasks'

  Scenario: The number shown at 'Manage Subtasks' button of a Task should be equal to the number of Subtasks listed on it
    Given I am on the My Tasks page
    And I have a task listed
    Then I should see the same number os Subtasks listed as shown at 'Manage Subtasks' button

  ##----------------------------------------------
  ## Testing read only elements
  ##----------------------------------------------
  Scenario: The Subtask modal dialog should have a read only field with Task ID
    Given I am on the My Tasks page
    And I have a task listed
    When I click at 'Manage Subtasks' button
    Then I should see a read only field with Task ID

  @Bug
  Scenario: The Subtask modal dialog should have a read only field with Task description
    Given I am on the My Tasks page
    And I have a task listed
    When I click at 'Manage Subtasks' button
    Then I should see a read only field with Task description

  ##----------------------------------------------
  ## Testing SubTask Description limits
  ##----------------------------------------------
  Scenario: The 'SubTask Description' field allows 250 characters
    Given I am on the My Tasks page
    And I have a task listed
    When I click at 'Manage Subtasks' button
    Then I should be able to input 250 characters at 'SubTask Description' field

  @Bug
  Scenario: The 'SubTask Description' field does not allow more than 250 characters
    Given I am on the My Tasks page
    And I have a task listed
    When I click at 'Manage Subtasks' button
    Then I should not be able to input 251 characters at 'SubTask Description' field

  ##----------------------------------------------
  ## Testing inclusion of subtasks
  ##----------------------------------------------
  Scenario: Add a Subtask filling all required fields
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date       |
      | New SubTask | 10/06/2017 |
    Then I should see a new subtask with description "New SubTask" appended on the botton part of modal

  @Bug 
  Scenario: Add a Subtask with a blank description
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date       |
      |             | 10/06/2017 |
    Then I should not see a new subtask with description "empty" appended on the botton part of modal

  @Bug 
  Scenario: Add a Subtask with a blank Due Date
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date |
      | New SubTask |      |
    Then I should not see a new subtask with description "New SubTask" appended on the botton part of modal

  ##----------------------------------------------
  ## Testing inclusion of subtasks with specific dates
  ##----------------------------------------------
  @Bug
  Scenario: Add a Subtask with invalid year format at Due Date field
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date     |
      | New SubTask | 06/30/17 |
    Then I should not see a new subtask with description "New SubTask" appended on the botton part of modal

  @Bug   
  Scenario: Add a Subtask with invalid month at Due Date field
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date       |
      | New SubTask | 30/30/2017 |
    Then I should not see a new subtask with description "New SubTask" appended on the botton part of modal

  @Bug
  Scenario: Add a Subtask with invalid day at Due Date field
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date       |
      | New SubTask | 02/32/2017 |
    Then I should not see a new subtask with description "New SubTask" appended on the botton part of modal

  
  Scenario: Add a Subtask with leap year at Due Date field
    Given I am on the My Tasks page
    And I have a task listed
    When I add a new subtask with the characteristics
      | description | date       |
      | New SubTask | 02/29/2020 |
    Then I should see a new subtask with description "New SubTask" appended on the botton part of modal
