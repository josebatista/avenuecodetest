package br.com.avenuecode.avenuecodetest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith (Cucumber.class)
@CucumberOptions ( snippets=SnippetType.CAMELCASE ,
plugin="pretty",glue = "br.com.avenuecode.steps", 
features="classpath:", tags="@Bug")
public class RunTest {

}
