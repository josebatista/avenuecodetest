# README #



This is the admission test for AvenueCode. This is a simple project of Automated UI Tests built in Java based on User Stories available at [http://qa-test.avenuecode.com/UserStories.pdf](http://qa-test.avenuecode.com/UserStories.pdf). 

All the scenarios performed, even manually, are available at the [src/main/resources](src/main/resources) folder.

Only the scenarios which bugs were found have code implementation. All these scenarios have a **Bug** tag in the feature file.

The PDF file describing types and techniques of testing is available at the [docs](docs) folder.


### Tools ###
The following tools were used in the project:

* Cucumber-JVM
* Selenium
* Maven
* Pico Container

### Requirements ###

* Google Chrome 58-60v.
* Maven
* JDK 8+

### Running ###

As every implemented tests have a **Bug** tag, then you only need to run the following Maven command on the root project folder:

```mvn test -Dcucumber.options="--tags @Bug"```

